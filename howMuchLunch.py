import math

def convertToHours(n):
    return math.floor(n)

def convertToMinutes(n):
    return math.floor((n % 1) * 60)

print("How Much Lunch Do I Have?")

HOURS_IN_WEEK = 40

answer = input('Options:\n1) How much lunch do I have left?\n2) How much lunch do I have left if I take this much today?\n')

if answer == '1':
    daysLeft = float(input('How many days are left? '))
    weeklyHourTotal = float(input('What is your weekly hour total? '))

    hoursLeftToWork = HOURS_IN_WEEK - weeklyHourTotal
    print('hoursLeftToWork: ' + str(hoursLeftToWork) + ' OR ' + str(convertToHours(hoursLeftToWork)) + ' hours ' + str(convertToMinutes(hoursLeftToWork)) + ' minutes.')

    totalLunchLeft = (hoursLeftToWork - (((daysLeft - 1) * 9) + 8)) * -1
    print('totalLunchLeft: ' + str(totalLunchLeft) + ' OR ' + str(convertToHours(totalLunchLeft)) + ' hours ' + str(convertToMinutes(totalLunchLeft)) + ' minutes.')

    avgLunchPerDay = totalLunchLeft / daysLeft
    print('avgLunchPerDay: ' + str(avgLunchPerDay) + ' OR ' + str(convertToHours(avgLunchPerDay)) + ' hours ' + str(convertToMinutes(avgLunchPerDay)) + ' minutes.')

elif answer == '2':
    daysLeft = float(input('How many days are left after this lunch has been taken? '))
    weeklyHourTotal = float(input('What is your weekly hour total? '))
    howMuchLunchToday = float(input('How much lunch are you taking today? '))

    hoursLeftToWork = HOURS_IN_WEEK - weeklyHourTotal - (9 - howMuchLunchToday)
    print('hoursLeftToWork: ' + str(hoursLeftToWork) + ' OR ' + str(convertToHours(hoursLeftToWork)) + ' hours ' + str(convertToMinutes(hoursLeftToWork)) + ' minutes.')

    totalLunchLeft = (hoursLeftToWork - (((daysLeft - 1) * 9) + 8)) * -1
    print('totalLunchLeft: ' + str(totalLunchLeft) + ' OR ' + str(convertToHours(totalLunchLeft)) + ' hours ' + str(convertToMinutes(totalLunchLeft)) + ' minutes.')

    avgLunchPerDay = totalLunchLeft / daysLeft
    print('avgLunchPerDay: ' + str(avgLunchPerDay) + ' OR ' + str(convertToHours(avgLunchPerDay)) + ' hours ' + str(convertToMinutes(avgLunchPerDay)) + ' minutes.')
else:
    print('Not a valid option.')
    